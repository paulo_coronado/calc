package com.example.ultracalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //1. Crear los objetos que se "relacionarán" con los botones de la layout

    MaterialButton boton1, boton2, boton3,boton4, boton5,boton6,boton7,boton8,boton9,boton0;

    TextView expresion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        expresion=findViewById(R.id.expresion);

        //2. Relacionar el objeto boton1 con el elemento cuyo id sea boton_1
        boton1=findViewById(R.id.boton_1);
        boton2=findViewById(R.id.boton_2);
        boton3=findViewById(R.id.boton_3);
        boton4=findViewById(R.id.boton_4);
        boton5=findViewById(R.id.boton_5);
        boton6=findViewById(R.id.boton_6);
        boton7=findViewById(R.id.boton_7);
        boton8=findViewById(R.id.boton_8);
        boton9=findViewById(R.id.boton_9);
        boton0=findViewById(R.id.boton_0);




        //3. Convertir el boton1 en un listener
        boton1.setOnClickListener(this);
        boton2.setOnClickListener(this);
        boton3.setOnClickListener(this);
        boton4.setOnClickListener(this);
        boton5.setOnClickListener(this);
        boton6.setOnClickListener(this);
        boton7.setOnClickListener(this);
        boton8.setOnClickListener(this);
        boton9.setOnClickListener(this);
        boton0.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {

        //1. Crear un objeto que "represente" el botón al cual se le ha hecho click
        MaterialButton boton= (MaterialButton) view;

        String texto;

        texto= boton.getText().toString();

        this.expresion.setText(texto);





    }
}